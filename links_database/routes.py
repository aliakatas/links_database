from flask import render_template, flash, redirect, url_for
from links_database import app
from links_database.forms import NewLinkForm

@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')

@app.route('/addlink', methods=['GET', 'POST'])
def addlink():
    form = NewLinkForm()
    if form.validate_on_submit():
        flash('Link {} added'.format(form.thelink.data))
        return redirect(url_for('index'))
    return render_template('add_link.html', title='Add Link', form=form)