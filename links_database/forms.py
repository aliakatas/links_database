from flask_wtf import FlaskForm
from wtforms import StringField, DateField, SubmitField
from wtforms.validators import DataRequired

class NewLinkForm(FlaskForm):
    thelink = StringField('Link', validators=[DataRequired()])
    submit = SubmitField('Add')